from models.account import Account
from models.user import User
from models.credentials import Credentials
from models.keywords import Keywords
from models.stk_push_transactions import STKPushTransactions
from models.c2b_transactions import C2BTransactions
from models.banks import Banks


class Bees:

    def __init__(self, key: str, secret: str):
        self.key = key
        self.secret = secret

    def get_user_factory(self) -> User:
        return User(self.key, self.secret)

    def get_account_factory(self) -> Account:
        return Account(self.key, self.secret)

    def get_credentials_factory(self) -> Credentials:
        return Credentials(self.key, self.secret)

    def get_keywords_factory(self) -> Keywords:
        return Keywords(self.key, self.secret)

    def get_banks_factory(self) -> Banks:
        return Banks(self.key, self.secret)

    def get_stkpushtransactions_factory(self) -> STKPushTransactions:
        return STKPushTransactions(self.key, self.secret)

    def get_c2btransactions_factory(self) -> C2BTransactions:
        return C2BTransactions(self.key, self.secret)
