import requests
import secrets
import hmac
import hashlib
import base64
import json


def mpesa_stk_push_example():

    # ---------- Edit this section only -----------------
    payer = 254703133896
    amount = 100
    displayed_description = 'Sweater'
    full_description = 'Payment for Sweater'
    # ---------------------------------------------------

    method = 'POST'
    url = 'http://local-user.bees.co.ke'
    path = '/v1/promptSTKPushPayment'
    content_type = 'application/json'
    date = 'Thu, 17 Jan 2019 18:48:58 GMT'
    key = 'deccf75f6e941e95df6073497214c266'
    secret = '0367a85eb3859cf8e5de1683e5306bdb'

    payload = {
        "payment_type": "mpesa_stk_push",
        "payer": payer,
        "callback_url": "http://d353bee1.ngrok.io/mpesa/result_test",
        "displayed_desc": displayed_description,
        "full_desc": full_description,
        "amount": amount,
        "account_ref": "6374bfa8b3db023d"
    }

    content = json.dumps(payload).encode('utf-8')
    md5 = hashlib.md5(content).hexdigest().upper()
    nonce = generate_nonce()
    hmac_hash = generate_hmac(secret, method, path, md5, content_type, date, nonce)

    headers = {
        "Authorization": "BEES {}:{}".format(key, hmac_hash),
        "Content-type": content_type,
        "Content-MD5": md5,
        "Date": date,
        "nonce": nonce
    }

    response = requests.post("{}{}".format(url, path), data=content, headers=headers)
    print(response.json())


def generate_nonce():
    return secrets.token_hex(15)


def generate_hmac(secret, method, path, md5, content_type, date, nonce):
    content_str = method.upper() + path + md5 + content_type + date + nonce
    message = content_str.encode()
    return base64.b64encode(hmac.new(secret.encode(), message, hashlib.sha256).hexdigest().encode()).decode("utf-8")


mpesa_stk_push_example()
