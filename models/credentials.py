from models.base import Base


class Credentials(Base):

    def __init__(self, key: str, secret: str):
        super().__init__(key, secret)
        self.credentials_path = "/v1/credentials"

    def get_credentials(self):
        return self.get(self.credentials_path, '', self.content_type)

    def activate_credentials(self, credentials_ref: str):
        endpoint = '{}/{}/activate'.format(self.credentials_path, credentials_ref)
        return self.put(endpoint, None, self.content_type)

    def deactivate_credentials(self, credentials_ref: str):
        endpoint = '{}/{}/deactivate'.format(self.credentials_path, credentials_ref)
        return self.put(endpoint, None, self.content_type)
