from utils.env import Env
from models.base import Base
from utils.payment_status_type import PaymentStatusType


class C2BTransactions(Base):

    def __init__(self, key: str, secret: str):
        super().__init__(key, secret)
        self.c2b_poll_transactions_path = "/v1/transactions"

    def poll_transaction(self, payment_status_type: PaymentStatusType, transaction_id: str,
                         callback_url: str, env: Env) -> str:
        params = self.create_transaction_params_path(payment_status_type, transaction_id,
                                                     callback_url, env)
        return self.get(self.c2b_poll_transactions_path, params, self.content_type)

    def create_transaction_params_path(self, payment_status_type: PaymentStatusType, transaction_id: str,
                                       callback_url: str, env: Env) -> str:
        return 'transaction_id={}&payment_status_type={}&callback_url={}&env={}'.format(transaction_id,
                                                                                        payment_status_type.name,
                                                                                        callback_url, env.name)
