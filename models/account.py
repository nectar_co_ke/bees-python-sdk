from models.base import Base


class Account(Base):

    def __init__(self, key: str, secret: str):
        super().__init__(key, secret)
        self.account_path = "/v1/account"

    def create_account(self, bank_ref: str, account_no: str):
        payload = self.create_payload(self.create_account_params(bank_ref, account_no))
        return self.post(self.account_path, payload, self.content_type)

    def get_account(self, account_ref: str):
        path_args = 'ref={}'.format(account_ref)
        return self.get(self.account_path, path_args, self.content_type)

    def update_account(self, account_ref: str, bank_ref: str, account_no: str):
        payload = self.create_payload(self.create_update_account_params(account_ref, bank_ref, account_no))
        return self.put(self.account_path, payload, self.content_type)

    def deactivate_account(self, account_ref: str):
        path_args = 'account_ref={}'.format(account_ref)
        return self.delete(self.account_path, path_args, self.content_type)

    def create_account_params(self, bank_ref: str, account_no: str) -> dict:
        return {
            'bank_ref': bank_ref,
            'account_no': account_no
        }

    def create_update_account_params(self, account_ref: str, bank_ref: str, account_no: str) -> dict:
        return {
            'account_ref': account_ref,
            'bank_ref': bank_ref,
            'account_no': account_no
        }
