from models.base import Base


class User(Base):

    def __init__(self, key: str, secret: str):
        super().__init__(key, secret)
        self.user_path = "/v1/user"

    def create_user(self, first_name: str, last_name: str, username: str,
                    password: str, phone_no: str, email: str, image_url: str):
        payload = self.create_payload(self.create_user_params(first_name, last_name, username,
                                                              password, phone_no, email, image_url))
        return self.post(self.user_path, payload, self.content_type)

    def get_user(self, username: str):
        return self.get(self.user_path, 'username={}'.format(username), self.content_type)

    def update_user(self, ref: str, first_name: str, last_name: str, username: str,
                    password: str, phone_no: str, email: str, image_url: str):
        payload = self.create_payload(self.create_update_user_params(ref, first_name, last_name, username,
                                                                     password, phone_no, email, image_url))
        return self.put(self.user_path, payload, self.content_type)

    def delete_user(self, user_ref: str):
        params = 'ref={}'.format(user_ref)
        return self.delete(self.user_path, params, self.content_type)

    def create_user_params(self, first_name: str, last_name: str, username: str,
                           password: str, phone_no: str, email: str, image_url: str) -> dict:
        return {
            'first_name': first_name,
            'last_name': last_name,
            'username': username,
            'password': password,
            'phone_no': phone_no,
            'email': email,
            'image_url': image_url
        }

    def create_update_user_params(self, ref: str, first_name: str, last_name: str, username: str,
                                  password: str, phone_no: str, email: str, image_url: str) -> dict:
        update_user_params = {
            'ref': ref
        }
        user_params = self.create_user_params(first_name, last_name, username, password,
                                              phone_no, email, image_url)
        return {**update_user_params, **user_params}
