from utils.env import Env
from utils.payment_type import PaymentType
from utils.payment_status_type import PaymentStatusType
from models.base import Base


class STKPushTransactions(Base):

    def __init__(self, key: str, secret: str):
        super().__init__(key, secret)
        self.stk_push_poll_transactions_path = "/v1/stktransactions"
        self.prompt_payment_transactions_path = "/v1/promptSTKPushPayment"

    def prompt_stkpush_transaction(self, payment_type: PaymentType, payer: str,
                                   callback_url: str, displayed_desc: str, full_desc: str,
                                   amount: float, account_ref: str, env: Env) -> str:
        payload = self.create_payload(self.create_stkpush_transaction_params(payment_type, payer, callback_url,
                                                                             displayed_desc, full_desc, amount,
                                                                             account_ref, env))
        return self.post(self.prompt_payment_transactions_path, payload, self.content_type)

    def poll_stkpush_transaction(self, payment_status_type: PaymentStatusType, transaction_ref: str) -> str:
        return self.get(self.stk_push_poll_transactions_path,
                        self.create_stkpush_poll_transaction_path(payment_status_type, transaction_ref),
                        self.content_type)

    def create_stkpush_transaction_params(self, payment_type: PaymentType, payer: str,
                                          callback_url: str, displayed_desc: str, full_desc: str,
                                          amount: float, account_ref: str, env: Env) -> dict:
        return {
            'payment_type': payment_type.name,
            'payer': payer,
            'callback_url': callback_url,
            'displayed_desc': displayed_desc,
            'full_desc': full_desc,
            'amount': amount,
            'account_ref': account_ref,
            'env': env.name
        }

    def create_stkpush_poll_transaction_path(self, payment_status_type: PaymentStatusType, transaction_ref: str) -> str:
        return 'payment_status_type={}&ref={}'.format(payment_status_type.name, transaction_ref)
