from models.base import Base
from utils.env import Env
from collections import defaultdict


class Keywords(Base):

    def __init__(self, key: str, secret: str):
        super().__init__(key, secret)
        self.keywords_path = "/v1/keywords"

    def create_keywords(self, keywords: list, confirmation_url: str, env: Env):
        payload = self.create_payload(self.create_keywords_params(keywords, confirmation_url, env))
        return self.post(self.keywords_path, payload, self.content_type)

    def get_keywords(self):
        return self.get(self.keywords_path, '', self.content_type)

    def delete_keyword(self, keyword_ref: str):
        endpoint = '{}/{}'.format(self.keywords_path, keyword_ref)
        return self.delete(endpoint, '', self.content_type)

    def create_keywords_params(self, keywords: list, confirmation_url: str, env: Env) -> dict:
        params = defaultdict(str)
        params['keywords'] = ','.join(keywords)
        params['confirmation_url'] = confirmation_url
        params['env'] = env.name

        return params
