from models.base import Base


class Banks(Base):

    def __init__(self, key: str, secret: str):
        super().__init__(key, secret)
        self.banks_path = "/v1/banks"

    def get_banks(self):
        return self.get(self.banks_path, '', self.content_type)
