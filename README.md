# Bees Python SDK

Bees provides the quickest way to integrate with the MPESA {daraja} SDKs. This repository contains the Python3.6 wrapper API around Bees REST MPESA API.

## Getting Started

Please ensure that you have Python3.6 installed to use these bindings.

### Examples


```
key = '5ce0189ba8ce979e354c280560988c49'
secret = '4185ec490c5dd1da62688554c291641f'

bees = Bees(key, secret)

# create user
print(bees.get_user_factory().create_user(first_name='First', last_name='Last', username='first', password='password',
                                          phone_no='0703133800', email='first@gmail.com',
                                          image_url='https://bees-avatar-images.s3.amazonaws.com/1557909926.jpeg'))

# get user
print(bees.get_user_factory().get_user(username='first'))

# update user
print(bees.get_user_factory().update_user(ref='a51d5ecddfa5041096ea0c37e6de1fdf', first_name='New First',
                                          last_name='New Last', username='first',
                                          password='password', phone_no='0703133800', email='first@gmail.com',
                                          image_url='https://bees-avatar-images.s3.amazonaws.com/1557909926.jpeg'))

# delete user
print(bees.get_user_factory().delete_user(user_ref='a51d5ecddfa5041096ea0c37e6de1fdf'))

# get keywords
print(bees.get_keywords_factory().get_keywords())

# create keywords
print(bees.get_keywords_factory().create_keywords(keywords=['GOAL', 'KICK'],
                                                  confirmation_url='https://bees.software/log.php', env=Env.live))

# delete keywords
print(bees.get_keywords_factory().delete_keyword(keyword_ref='a0952910c222a6f1b67987fd34a9c408'))

# get banks
print(bees.get_banks_factory().get_banks())

# create account
print(bees.get_account_factory().create_account(bank_ref='3ec0192b733cd6d65bd9f5543dd41e5f', account_no='8489542854'))

# get account
print(bees.get_account_factory().get_account(account_ref='c26b643a61ba81b2498e5c887e83da63'))

# deactivate account
print(bees.get_account_factory().deactivate_account(account_ref='c26b643a61ba81b2498e5c887e83da63'))

# get credentials
print(bees.get_credentials_factory().get_credentials())

# activate credentials
print(bees.get_credentials_factory().activate_credentials(credentials_ref='984c1e6735cc32583308ff3df675c28b'))

# deactivate credentials
print(bees.get_credentials_factory().deactivate_credentials(credentials_ref='984c1e6735cc32583308ff3df675c28b'))

# prompt stk push
print((bees.get_stkpushtransactions_factory().prompt_stkpush_transaction(payment_type=PaymentType.mpesa_stk_push,
                                                                         payer='254703133896',
                                                                         callback_url='https://bees.software/log.php',
                                                                         displayed_desc='sweater',
                                                                         full_desc='Payment for sweater', amount=100,
                                                                         account_ref='c26b643a61ba81b2498e5c887e83da63',
                                                                         env=Env.live)))

# poll stk push
print(bees.get_stkpushtransactions_factory().poll_stkpush_transaction(
    payment_status_type=PaymentStatusType.mpesa_stk_poll,
    transaction_ref='b871e5878b6b627f3b80756b19e0ae8c'))

# poll c2b transactions
print(bees.get_c2btransactions_factory().poll_transaction(payment_status_type=PaymentStatusType.mpesa_poll,
                                                          transaction_id='NEU998F3UJ',
                                                          callback_url='https://bees.software/log.php', env=Env.live))
```

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

