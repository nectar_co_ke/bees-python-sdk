from enum import Enum


class PaymentStatusType(Enum):
    mpesa_stk_poll = 1
    mpesa_poll = 2
